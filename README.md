# My Neovim configuration for JavaScript development

My personal configuration for Neovim for writing JavaScript.

This repo is not ready for an out-of-the-box experience from cloning. Use with discretion.
