nnoremap <silent><buffer><expr> <CR>
      \ defx#is_directory() ?
      \ defx#do_action('open_or_close_tree') :
      \ defx#do_action('open', 'vsplit')
nnoremap <silent><buffer><expr> Nd
      \ defx#do_action('new_directory')
nnoremap <silent><buffer><expr> Nf
      \ defx#do_action('new_file')
nnoremap <silent><buffer><expr> dd
      \ defx#do_action('remove')
nnoremap <silent><buffer><expr> R
      \ defx#do_action('rename')
nnoremap <silent><buffer><expr> q
      \ defx#do_action('quit')
nnoremap <silent><buffer><expr> mv
      \ defx#do_action('move')
nnoremap <silent><buffer><expr> cp
      \ defx#do_action('copy')
nnoremap <silent><buffer><expr> P
      \ defx#do_action('paste')
nnoremap <silent><buffer><expr> s
      \ defx#do_action('toggle_select')
nnoremap <silent><buffer><expr> S
      \ defx#do_action('toggle_select_all')
vnoremap <silent><buffer><expr> S
      \ defx#do_action('toggle_select_visual')
nnoremap <silent><buffer><expr> Y
      \ defx#do_action('yank_path')
nnoremap <silent><buffer><expr> > defx#do_action('resize',
      \ defx#get_context().winwidth + 10)
nnoremap <silent><buffer><expr> < defx#do_action('resize',
      \ defx#get_context().winwidth - 10)

nnoremap <silent><buffer><expr> <Space>st
      \ defx#do_action('toggle_sort', 'time')

autocmd BufWritePost * call defx#redraw()
