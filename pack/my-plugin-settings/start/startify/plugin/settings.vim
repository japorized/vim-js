let g:ascii_header = [
\ '        _              __                  _ ____  ',
\ ' __   _(_)_ __ ___    / _| ___  _ __      | / ___| ',
\ ' \ \ / / | ''_ '' _ \  | |_ / _ \| ''__|  _  | \___ \ ',
\ '  \ V /| | | | | | | |  _| (_) | |    | |_| |___) |',
\ '   \_/ |_|_| |_| |_| |_|  \___/|_|     \___/|____/ ',
\ ]
let g:startify_custom_header = 'map(g:ascii_header, "\"   \".v:val")'
let g:startify_enable_special = 0
let g:startify_lists = [
      \ { 'type': 'sessions',  'header': ['   Sessions']       },
      \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
      \ { 'type': 'files',     'header': ['   MRU']            },
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ { 'type': 'commands',  'header': ['   Commands']       },
      \ ]
let g:startify_commands = [
    \ {'S': ['Simplenote', ':SimplenoteList']},
    \ ]
let g:startify_session_dir = '~/.data/js-vim/session'
