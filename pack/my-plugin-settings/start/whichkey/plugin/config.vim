call which_key#register('<Space>', "g:which_key_map")
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
vnoremap <silent> <leader> :WhichKeyVisual '<Space>'<CR>
nnoremap <silent> <leader>F :Defx -toggle -split=vertical -winwidth=30 -direction=topleft<CR>
nmap <Leader>f <Plug>(PrettierAsync)
vmap <Leader>f <Plug>(PrettierPartial)
nmap <Leader>j :Denite -start-filter -split=floating file/rec<CR>
nmap <Leader>Js :Denite -start-filter -split=floating -default-action=split file/rec<CR>
nmap <Leader>Jv :Denite -start-filter -split=floating -default-action=vsplit file/rec<CR>
nmap <Leader>Jt :Denite -start-filter -split=floating -default-action=tabopen file/rec<CR>
nmap <Leader>/ :Denite -start-filter grep:. -no-empty<CR>
vnoremap <Leader>/ :DeniteCursorWord -start-filter grep:. -no-empty<CR>

let g:which_key_map = {
      \ 'F': 'Defx',
      \ 'f': 'Prettier',
      \ 'j': 'Jump',
      \ 'J': {
      \   'name': '+Jump',
      \   'v': 'Jump and vsplit',
      \   's': 'Jump and split',
      \   't': 'Jump and tabnew',
      \   },
      \ '/': 'Find file by content pattern',
      \ }

  " Document Related
nmap <Leader>dsi :IndentLinesToggle<CR>
nmap <silent> <leader>dst :call TypeWriterToggle()<CR>
nmap <silent> <leader>dsc :call ConcealToggle()<CR>
nmap <silent> <leader>dsn :call LineNumberToggle()<CR>
nmap <silent> <leader>dsr :call RelNumberToggle()<CR>
nmap <silent> <leader>dsst :SignifyToggle<CR>
nmap <silent> <leader>dssr :SignifyRefresh<CR>
nmap <silent> <leader>dl :set tw=
nmap <silent> <leader>dT :Tabularize /
vmap <leader>dcch :CamelToHyphenSel<CR>
vmap <leader>dccs :CamelToSnakeSel<CR>
vmap <leader>dchc :HyphenToCamelSel<CR>
vmap <leader>dchs :HyphenToSnakeSel<CR>
vmap <leader>dcsh :SnakeToHyphenSel<CR>
vmap <leader>dcsc :SnakeToCamelSel<CR>
nmap <leader>dtg :Denite -split=floating gettemplates<CR>
nmap <leader>dte :Denite -split=floating edittemplates<CR>
nnoremap <leader>ddw :%s/\s\+$//e<CR>
let g:which_key_map.d = {
      \ 'name' : '+document',
      \ 'l' : 'textwidth',
      \ 's' : {
      \   'name' : '+viewer-styling',
      \   'c' : 'toggle-conceal',
      \   'i' : 'toggle-indentline',
      \   'n': 'line-number-toggle',
      \   'r': 'relative-number-toggle',
      \   's' : {
      \     'name' : '+Signify',
      \     't' : 'signify-toggle',
      \     'r' : 'signify-refresh'
      \     },
      \   't' : 'toggle-typewriter-mode'
      \   },
      \ 'T' : 'Tabularize',
      \ 'c' : {
        \ 'name' : '+case-convert',
        \ 'c' : {
          \ 'name' : '+from-camel',
          \ 'h' : 'camel-to-hyphen',
          \ 's' : 'camel-to-snake',
        \   },
        \ 'h' : {
          \ 'name' : '+from-hyphen',
          \ 'c' : 'hyphen-to-camel',
          \ 's' : 'hyphen-to-snake',
        \   },
        \ 's' : {
          \ 'name' : '+from-snake',
          \ 'c' : 'snake-to-camel',
          \ 'h' : 'snake-to-hyphen',
        \   },
      \   },
      \ 't' : {
      \   'name' : '+template',
      \   'g' : 'get-template',
      \   'e' : 'edit-template',
      \   },
      \ 'd' : {
      \   'name' : '+delete-actions',
      \   'w' : 'remove-trailing-whitespaces'
      \   }
      \ }
let g:typewriter_mode = 0
let g:linenumber_mode = 0
let g:relnumber_mode = 0
function! TypeWriterToggle()
  if g:typewriter_mode
    set scrolloff=0
    let g:typewriter_mode = 0
  else
    set scrolloff=999
    let g:typewriter_mode = 1
  endif
endfunction
function! ConcealToggle()
  if &conceallevel == 0
    set conceallevel=2
  else
    set conceallevel=0
  endif
endfunction
function! LineNumberToggle()
  set nornu
  let g:relnumber_mode = 0
  if g:linenumber_mode
    set nonu
    let g:linenumber_mode = 0
  else
    set nu
    let g:linenumber_mode = 1
  endif
endfunction
function! RelNumberToggle()
  set nonu
  let g:linenumber_mode = 0
  if g:relnumber_mode
    set nornu
    let g:relnumber_mode = 0
  else
    set rnu
    let g:relnumber_mode = 1
  endif
endfunction

" {{{ Window management
nmap <silent> <leader>wse :split<Space>
nmap <silent> <leader>wst :split term://zsh<CR>a
nmap <silent> <leader>wve :vsplit<Space>
nmap <silent> <leader>wvt :vsplit term://zsh<CR>a
nmap <silent> <leader>wtc :tabnew %<CR>
nmap <silent> <leader>wte :tabnew<Space>
nmap <silent> <leader>wtt :tabnew term://zsh<CR>a
let g:which_key_map.w = { 
      \'name' : '+window',
      \ 's' : {
      \   'name' : '+split-options',
      \   'c' : ['<C-W>s', 'Split current buffer'],
      \   'e' : 'Split to edit a file',
      \   'd' : ['<C-W>d', 'Split and jump to defn'],
      \   'f' : ['<C-W>f', 'Split and edit filename under cursor'],
      \   'i' : ['<C-W>i', 'Split and jump to identifier'],
      \   'n' : [':new',  'Split a new window'],
      \   't' : 'Split a new terminal',
      \   },
      \ 'v' : {
      \   'name' : '+vsplit-options',
      \   'c' : ['<C-W>v', 'Vsplit current buffer'],
      \   'e' : 'Vsplit to edit a file',
      \   'n' : [':vnew',  'Vsplit a new window'],
      \   't' : 'Vsplit a new terminal',
      \   },
      \ 't' : {
      \   'name' : '+tabnew-options',
      \   'c' : 'Open current buffer in new tab',
      \   'e' : 'open-in-new-tab',
      \   'n' : ['tabnew', 'Open empty new tab'],
      \   'f' : ['<C-W>gF', 'Edit filename under cursor in new tab'],
      \   't' : 'Open terminal in new tab',
      \   },
      \ 'r': {
      \   'name': '+resize',
      \   'h': ['<C-W>5<', 'Expand window leftwards'],
      \   'H': ['<C-W>15<', 'Expand window further leftwards'],
      \   'j': ['5<C-W>+', 'Expand window downwards'],
      \   'J': ['15<C-W>+', 'Expand window further downwards'],
      \   'k': ['5<C-W>-', 'Expand window upwards'],
      \   'K': ['15<C-W>-', 'Expand window further upwards'],
      \   'l': ['<C-W>5>', 'Expand window rightwards'],
      \   'L': ['<C-W>5>', 'Expand window further rightwards'],
      \   '=': ['<C-W>=', 'Equalize window sizes'],
      \    },
      \ 'm': {
      \   'name': '+move',
      \   'H': ['<C-W>H', 'Current window to far left'],
      \   'J': ['<C-W>J', 'Current window to very bottom'],
      \   'K': ['<C-W>K', 'Current window to very top'],
      \   'L': ['<C-W>L', 'Current window to far right'],
      \   'T': ['<C-W>T', 'Current window to new tab'],
      \   },
      \ 'w': [':call WindowSwap#EasyWindowSwap()', 'Window swap'],
      \ }
" }}}

" {{{ Startify
nmap <Leader>St :Startify<CR>
nmap <Leader>SS :SSave!<CR>
nmap <leader>Ss :call LazySave()<CR>
nmap <Leader>Sd :SDelete!<CR>
nmap <Leader>Sc :SClose<CR>
let g:which_key_map.S = {
      \ 'name' : '+startify',
      \ 't' : 'Startify',
      \ 's' : 'Quicksave session',
      \ 'd' : 'SDelete',
      \ 'c' : 'Save & Close',
      \ }
function! LazySave() abort
  if filereadable(v:this_session)
    execute "normal!:SSave!<CR><CR>"
    echo "Session saved"
  else
    :SSave
  endif
endfunction
" }}}

" {{{ Buffer conttrols
nmap <Leader>bb :Denite -start-filter -split=floating buffer<CR>
nmap <Leader>bl :ls<CR>
nmap <Leader>bp :bl<CR>
nmap <Leader>bn :bn<CR>
nmap <Leader>bp :bp<CR>
nmap <Leader>bsl :sbl<CR>
nmap <Leader>bsf :sbf<CR>
nmap <Leader>bsn :sbn<CR>
nmap <Leader>bsp :sbp<CR>
nmap <Leader>bd :bdelete<Space>
nmap <Leader>bD :bdelete! %<CR>
let g:which_key_map.b = {
      \ 'name' : '+buffer',
      \ 'b' : 'buffer-goto',
      \ 'l' : 'buffer-list',
      \ 'd' : 'buffer-delete',
      \ 'D' : 'buffer-delete-current',
      \ 'n' : 'buffer-next',
      \ 'p' : 'buffer-prev',
      \ 's' : {
        \ 'name' : '+split',
        \ 'l' : 'buffer-split-last',
        \ 'f' : 'buffer-split-first',
        \ 'n' : 'buffer-split-next',
        \ 'p' : 'buffer-split-prev',
      \   },
      \ }
" }}}

" {{{ Iro
" This depends on a script file called iro, written by myself
vmap <leader>ih :call Iro("hex")<CR>
vmap <leader>ir :call Iro("rgba")<CR>
nmap <leader>id :call IroClean()<CR>
nmap <leader>il :call IroClear()<CR>
let g:which_key_map.i = {
  \ 'name' : '+iro',
  \ 'h' : 'show hex color',
  \ 'r' : 'show rgba color',
  \ 'd' : 'delete all cached colors',
  \ 'l' : 'remove all active palettes'
  \ }

function! Iro(type)
  if a:type == "hex"
    silent execute '!iro -p hex ' . shellescape(s:get_visual_selection(), 1)
  elseif a:type == "rgba"
    silent execute '!iro -p rgba ' . shellescape(s:get_visual_selection(), 1)
  endif
endfunction

function! IroClean()
  silent execute '!iro clean'
endfunction

function! IroClear()
  silent execute '!iro clear'
endfunction
" }}}

" {{{ CocActions
nmap <Leader>ac :call CocAction('pickColor')<CR>
xmap <silent> <leader>am :<C-u>execute 'CocCommand actions.open ' . visualmode()<CR>
nmap <silent> <leader>am :<C-u>set operatorfunc=<SID>cocActionsOpenFromSelected<CR>g@
nmap <Leader>aa :CocCommand eslint.executeAutofix<CR>
nmap <Leader>aR <Plug>(coc-rename)

let g:which_key_map.a = {
      \ 'name': '+code-actions',
      \ 'a': 'eslint autofix',
      \ 'c': 'change color',
      \ 'm': 'code actions menu',
      \ 'R': 'rename symbol',
      \ }

" Remap for do codeAction of selected region
function! s:cocActionsOpenFromSelected(type) abort
  execute 'CocCommand actions.open ' . a:type
endfunction
" }}}

" {{{ Config keybinds
  " {{{ Vim configurations
nnoremap <silent> <leader>cvei :tabnew $JSVIM_CONFIG_HOME/init.vim<CR>
nnoremap <silent> <leader>cvet :tabnew ~/.tmux.conf<CR>
nnoremap <silent> <leader>cveb :tabnew $JSVIM_CONFIG_HOME/pack/my-plugin-settings/start/whichkey/plugin/config.vim<CR>
nnoremap <silent> <leader>cvsi :source $JSVIM_CONFIG_HOME/init.vim<CR>
nnoremap <silent> <leader>cvsb :source $JSVIM_CONFIG_HOME/pack/my-plugin-settings/start/whichkey/plugin/config.vim<CR>
nnoremap <silent> <leader>cd :digraphs<CR>
nnoremap <silent> <leader>cscs :Denite -start-filter colorscheme<CR>
nnoremap <silent> <leader>cstb :call ToggleBackgroundMode()<CR>
nnoremap <silent> <leader>cH :call Helptags()<CR>
function! ToggleBackgroundMode() abort
  if &background == "light"
    set background=dark
  else
    set background=light
  endif
endfunction
function! Helptags() abort
  for glob in glob($JSVIM_CONFIG_HOME . '/pack/*/*/*/doc', '\n', 1)
    silent! execute 'helptags' glob
  endfor
  echom "Helptags generated!"
endfunction
  " }}}

  " {{{ Plugin - Coc
nnoremap <silent> <leader>cpcd :CocList diagnostics<CR>
nnoremap <silent> <leader>cpce :CocList extensions<CR>
nnoremap <silent> <leader>cpcl :CocList<CR>
nnoremap <silent> <leader>cpcc :CocConfig<CR>
nnoremap <silent> <leader>cpcE :CocEnable<CR>
nnoremap <silent> <leader>cpcD :CocDisable<CR>
nnoremap <silent> <leader>cpcR :CocRestart<CR>
nnoremap <silent> <leader>cpcs :CocStart<CR>
nnoremap <silent> <leader>cpcU :CocUpdate<CR>
nnoremap <silent> <leader>cpcI :CocInstall<Space>
nnoremap <silent> <leader>cpcX :CocUninstall<Space>
nnoremap <silent> <leader>cpcf :call FindCocExtensions()<CR>

function! FindCocExtensions() abort
  silent execute "!firefox 'https://www.npmjs.com/search?q=keywords:coc.nvim'"
endfunction
  " }}}

  " {{{ Plugin - Ultisnips
nnoremap <silent> <leader>cpuc :UltiSnipsEdit<CR>
nnoremap <silent> <leader>cpue :UltiSnipsEdit<Space>
  " }}}

  " {{{ Plugin - Vimspector
nnoremap <silent> <leader>vL :call vimspector#Launch()<CR>
nnoremap <leader>vc :call vimspector#Continue()<CR>
nnoremap <leader>vs :call vimspector#Stop()<CR>
nnoremap <leader>vp :call vimspector#Pause()<CR>
nnoremap <leader>vb :call vimspector#ToggleBreakpoint()<CR>
nnoremap <leader>vB :call vimspector#ToggleConditionalBreakpoint()<CR>
nnoremap <leader>vF :call vimspector#AddFunctionBreakpoint('<cexpr>')<CR>
nnoremap <leader>vR :VimspectorReset<CR>
nnoremap <leader>vS :VimspectorShowOutput 
nnoremap <leader>vo :call vimspector#StepOver()<CR>
nnoremap <leader>vi :call vimspector#StepInto()<CR>
nnoremap <leader>vO :call vimspector#StepOut()<CR>

let g:which_key_map.v = {
  \ 'name': '+vimspector',
  \ 'L': 'Launch debugger',
  \ 'c': 'Continue',
  \ 's': 'Stop',
  \ 'p': 'Pause',
  \ 'b': 'Toggle breakpoint',
  \ 'B': 'Toggle conditional breakpoint',
  \ 'F': 'Add function breakpoint',
  \ 'R': 'Reset',
  \ 'S': 'Show output for...',
  \ 'o': 'Step over',
  \ 'i': 'Step into',
  \ 'O': 'Step out',
  \ }
  " }}}

  " {{{ Mappings
let g:which_key_map.c = {
  \ 'name' : '+configs',
  \ 'd' : 'Show digraphs',
  \ 'p' : {
  \   'name' : '+plugins',
  \   'c': {
  \     'name' : '+coc',
  \     'd' : 'Diagnostics',
  \     'e' : 'Manage extensions',
  \     'l' : 'Lists',
  \     'c' : 'Edit config',
  \     'E' : 'Enable coc.nvim',
  \     'D' : 'Disable coc.nvim',
  \     'U' : 'Update coc (async)',
  \     'I' : 'Install extensions',
  \     'f' : 'Find extensions',
  \     },
  \   'u' : {
  \     'name' : '+Ultisnips',
  \     'c' : 'Edit current snippet',
  \     'e' : 'Edit chosen snippet',
  \     },
  \   },
  \ 'P' : {
  \   'name': '+plugin-manager',
  \   'd': 'Return unused plugin dirs',
  \   'D': 'Delete unused plugins',
  \   'E': 'Edit plugins file',
  \   'I': 'Install plugins',
  \   'l': 'Show logs',
  \   'L': 'List plugins',
  \   'p': 'Show progress',
  \   'S': 'Source plugins file',
  \   'u': 'Update plugins',
  \   },
  \ 's' : {
  \   'name' : '+styling',
  \   'c' : {
  \     'name' : '+colorscheme',
  \     'e' : 'Edit colorscheme',
  \     's' : 'Set colorscheme',
  \     },
  \   't' : {
  \     'name' : '+toggles',
  \     'b' : 'Toggle background light/dark',
  \     }
  \   },
  \ 'v' : {
  \   'name' : '+vim-configs',
  \   'e' : {
  \     'name' : '+edit-configs',
  \     'b' : 'Edit keybindings',
  \     'i' : 'Edit init',
  \     't' : 'Edit tmux config',
  \     },
  \   's' : {
  \     'name' : '+source-configs',
  \     'b' : 'Source keybindings',
  \     'i' : 'Source init',
  \     },
  \   },
  \ }
  " }}}
" }}}

" {{{ Utilities
vmap <leader>ucchr :call HexToRgba()<CR>
vmap <leader>uccrh :call RgbaToHex()<CR>
vmap <leader>uccrH :call RgbaToHsl()<CR>
vmap <leader>uccHr :call HslToRgba()<CR>
nmap <Leader>ug :tabnew term://lazygit<CR>a

let g:which_key_map.u = {
      \ 'name': '+utilities',
      \ 'c': {
      \   'name': '+color utils',
      \   'c': {
      \     'name': '+convert to clipboard',
      \     'h': {
      \       'name': '+from hex',
      \       'r': 'to rgba',
      \       },
      \     'r': {
      \       'name': '+from rgb(a)',
      \       'h': 'to hex',
      \       'H': 'to hsl',
      \       },
      \     'H': {
      \       'name': '+from hsl',
      \       'r': 'to rgba',
      \       },
      \     },
      \   },
      \ 'g': 'lazygit'
      \ }

function! HexToRgba()
  silent execute '!hex2rgba ' . shellescape(s:get_visual_selection(), 1) . ' | xclip -in -selection clipboard'
endfunction

function! RgbaToHex()
  silent execute '!rgba2hex ' . shellescape(s:get_visual_selection(), 1) . ' | xclip -in -selection clipboard'
endfunction

function! RgbToHsl()
  silent execute '!rgb2hsl ' . shellescape(s:get_visual_selection(), 1) . ' | xclip -in -selection clipboard'
endfunction

function! HslToRgba()
  silent execute '!hsl2rgb ' . shellescape(s:get_visual_selection(), 1) . ' | xclip -in -selection clipboard'
endfunction

" credits to this post on SE
" https://stackoverflow.com/a/6271254
function! s:get_visual_selection()
    " Why is this not a built-in Vim script function?!
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)
    if len(lines) == 0
        return ''
    endif
    let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]
    return join(lines, "\n")
endfunction
" }}}
