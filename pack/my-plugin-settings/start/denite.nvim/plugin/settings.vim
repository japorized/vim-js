call denite#custom#var('file/rec', 'command',
\ ['rg', '--files', '--hidden', '--glob', '!.git', '--color', 'never'])
" call denite#custom#kind('file', 'default_action', 'tabopen')

call denite#custom#var('grep', 'command', ['rg'])
call denite#custom#var('grep', 'default_opts', ['--hidden', '--vimgrep', '--heading', '-S'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])

" Remove date from buffer list
call denite#custom#var('buffer', 'date_format', '')

call denite#custom#source ('buffer', 'sorters', ['sorter/sublime', 'sorter/rank'])
